package Clases;

public class Accesorios {
	
	//Atributos
	private String _AccCuerda;
	private String _AccViento;
	private String _AccPercusi�n;
	private String _AccCuerdaFrotada;
	private String _AccPianos;
	
	//Constructor
	public Accesorios(String _AccCuerda, String _AccViento, String _AccPercusi�n, String _AccCuerdaFrotada,
			String _AccPianos) {
		super();
		this._AccCuerda = _AccCuerda;
		this._AccViento = _AccViento;
		this._AccPercusi�n = _AccPercusi�n;
		this._AccCuerdaFrotada = _AccCuerdaFrotada;
		this._AccPianos = _AccPianos;
	}
	
	//Get and Set
	public String get_AccCuerda() {
		return _AccCuerda;
	}
	public void set_AccCuerda(String _AccCuerda) {
		this._AccCuerda = _AccCuerda;
	}
	public String get_AccViento() {
		return _AccViento;
	}
	public void set_AccViento(String _AccViento) {
		this._AccViento = _AccViento;
	}
	public String get_AccPercusi�n() {
		return _AccPercusi�n;
	}
	public void set_AccPercusi�n(String _AccPercusi�n) {
		this._AccPercusi�n = _AccPercusi�n;
	}
	public String get_AccCuerdaFrotada() {
		return _AccCuerdaFrotada;
	}
	public void set_AccCuerdaFrotada(String _AccCuerdaFrotada) {
		this._AccCuerdaFrotada = _AccCuerdaFrotada;
	}
	public String get_AccPianos() {
		return _AccPianos;
	}
	public void set_AccPianos(String _AccPianos) {
		this._AccPianos = _AccPianos;
	}
	
}
