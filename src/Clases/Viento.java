package Clases;

public class Viento {
	//Atributos
	private String _Clarinete;
	private String _Saxof�n;
	private String _Oboe;
	private String _Tuba;
	private String _Tromb�n;
	private String _Acorde�n;
	private String _Arm�nica;
	private String _Flauta;
	private String _Trompeta;
	private String _FlautaTraversa;
	private String _Gaita;
	//Constructor
	public Viento(String _Clarinete, String _Saxof�n, String _Oboe, String _Tuba, String _Tromb�n, String _Acorde�n,
			String _Arm�nica, String _Flauta, String _Trompeta, String _FlautaTraversa, String _Gaita) {
		super();
		this._Clarinete = _Clarinete;
		this._Saxof�n = _Saxof�n;
		this._Oboe = _Oboe;
		this._Tuba = _Tuba;
		this._Tromb�n = _Tromb�n;
		this._Acorde�n = _Acorde�n;
		this._Arm�nica = _Arm�nica;
		this._Flauta = _Flauta;
		this._Trompeta = _Trompeta;
		this._FlautaTraversa = _FlautaTraversa;
		this._Gaita = _Gaita;	
	}
	
	//Get and Set
	public String get_Clarinete() {
		return _Clarinete;
	}
	public void set_Clarinete(String _Clarinete) {
		this._Clarinete = _Clarinete;
	}
	public String get_Saxof�n() {
		return _Saxof�n;
	}
	public void set_Saxof�n(String _Saxof�n) {
		this._Saxof�n = _Saxof�n;
	}
	public String get_Oboe() {
		return _Oboe;
	}
	public void set_Oboe(String _Oboe) {
		this._Oboe = _Oboe;
	}
	public String get_Tuba() {
		return _Tuba;
	}
	public void set_Tuba(String _Tuba) {
		this._Tuba = _Tuba;
	}
	public String get_Tromb�n() {
		return _Tromb�n;
	}
	public void set_Tromb�n(String _Tromb�n) {
		this._Tromb�n = _Tromb�n;
	}
	public String get_Acorde�n() {
		return _Acorde�n;
	}
	public void set_Acorde�n(String _Acorde�n) {
		this._Acorde�n = _Acorde�n;
	}
	public String get_Arm�nica() {
		return _Arm�nica;
	}
	public void set_Arm�nica(String _Arm�nica) {
		this._Arm�nica = _Arm�nica;
	}
	public String get_Flauta() {
		return _Flauta;
	}
	public void set_Flauta(String _Flauta) {
		this._Flauta = _Flauta;
	}
	public String get_Trompeta() {
		return _Trompeta;
	}
	public void set_Trompeta(String _Trompeta) {
		this._Trompeta = _Trompeta;
	}
	public String get_FlautaTraversa() {
		return _FlautaTraversa;
	}
	public void set_FlautaTraversa(String _FlautaTraversa) {
		this._FlautaTraversa = _FlautaTraversa;
	}
	public String get_Gaita() {
		return _Gaita;
	}
	public void set_Gaita(String _Gaita) {
		this._Gaita = _Gaita;
	}
	
}

