package Clases;

public class Cliente {
	
	//Atributos
	private String _Nombre;
	private int _DNI;
	private String _FormaPago;
	
	//Constructor
	public Cliente(String _Nombre, int _DNI, String _FormaPago) {
		super();
		this._Nombre = _Nombre;
		this._DNI = _DNI;
		this._FormaPago = _FormaPago;
	}
	
	//Get and Set
	public String get_Nombre() {
		return _Nombre;
	}
	public void set_Nombre(String _Nombre) {
		this._Nombre = _Nombre;
	}
	public int get_DNI() {
		return _DNI;
	}
	public void set_DNI(int _DNI) {
		this._DNI = _DNI;
	}
	public String get_FormaPago() {
		return _FormaPago;
	}
	public void set_FormaPago(String _FormaPago) {
		this._FormaPago = _FormaPago;
	}
	
	
	
	
}
