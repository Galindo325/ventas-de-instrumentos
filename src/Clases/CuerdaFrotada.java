package Clases;

public class CuerdaFrotada {
	
	//Atributos
	private String _Viol�n;
	private String _Contrabajo;
	private String _Viola;
	private String _Violonchelo;
	
	//Constructor
	public CuerdaFrotada(String _Viol�n, String _Contrabajo, String _Viola, String _Violonchelo) {
		super();
		this._Viol�n = _Viol�n;
		this._Contrabajo = _Contrabajo;
		this._Viola = _Viola;
		this._Violonchelo = _Violonchelo;
	}
	
	//Get and Set
	public String get_Viol�n() {
		return _Viol�n;
	}
	public void set_Viol�n(String _Viol�n) {
		this._Viol�n = _Viol�n;
	}
	public String get_Contrabajo() {
		return _Contrabajo;
	}
	public void set_Contrabajo(String _Contrabajo) {
		this._Contrabajo = _Contrabajo;
	}
	public String get_Viola() {
		return _Viola;
	}
	public void set_Viola(String _Viola) {
		this._Viola = _Viola;
	}
	public String get_Violonchelo() {
		return _Violonchelo;
	}
	public void set_Violonchelo(String _Violonchelo) {
		this._Violonchelo = _Violonchelo;
	}
	
	
}
