package Clases;

public class Cuerda {
	
	//Atributos
	private String _Arpa;
	private String _Guitarra;
	private String _Mandolina;
	private String _GuitarraElectrica;
	private String _Guitar�n;
	private String _Banjo;
	private String _La�d;
	private String _Bajo;
	
	//Constructor
	public Cuerda(String _Arpa, String _Guitarra, String _Mandolina, String _GuitarraElectrica, String _Guitar�n,
			String _Banjo, String _La�d, String _Bajo) {
		super();
		this._Arpa = _Arpa;
		this._Guitarra = _Guitarra;
		this._Mandolina = _Mandolina;
		this._GuitarraElectrica = _GuitarraElectrica;
		this._Guitar�n = _Guitar�n;
		this._Banjo = _Banjo;
		this._La�d = _La�d;
		this._Bajo = _Bajo;
	}
	
	//Set and Get
	public String get_Arpa() {
		return _Arpa;
	}
	public void set_Arpa(String _Arpa) {
		this._Arpa = _Arpa;
	}
	public String get_Guitarra() {
		return _Guitarra;
	}
	public void set_Guitarra(String _Guitarra) {
		this._Guitarra = _Guitarra;
	}
	public String get_Mandolina() {
		return _Mandolina;
	}
	public void set_Mandolina(String _Mandolina) {
		this._Mandolina = _Mandolina;
	}
	public String get_GuitarraElectrica() {
		return _GuitarraElectrica;
	}
	public void set_GuitarraElectrica(String _GuitarraElectrica) {
		this._GuitarraElectrica = _GuitarraElectrica;
	}
	public String get_Guitar�n() {
		return _Guitar�n;
	}
	public void set_Guitar�n(String _Guitar�n) {
		this._Guitar�n = _Guitar�n;
	}
	public String get_Banjo() {
		return _Banjo;
	}
	public void set_Banjo(String _Banjo) {
		this._Banjo = _Banjo;
	}
	public String get_La�d() {
		return _La�d;
	}
	public void set_La�d(String _La�d) {
		this._La�d = _La�d;
	}
	public String get_Bajo() {
		return _Bajo;
	}
	public void set_Bajo(String _Bajo) {
		this._Bajo = _Bajo;
	}
	
	
}
