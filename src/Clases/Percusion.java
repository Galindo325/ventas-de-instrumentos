package Clases;

public class Percusion {
	//Atributos
	private String _Tambor;
	private String _Timbal;
	private String _Xil�fono;
	private String _Campana;
	private String _Cr�talos;
	private String _Celesta;
	private String _Caj�n;
	private String _Tri�ngulo;
	private String _Taiko;
	private String _Casta�uelas;
	private String _Maracas;
	private String _Bombo;
	private String _Bater�a;
	private String _Gong;
	private String _Pandereta;
	
	//Constructor
	public Percusion(String _Tambor, String _Timbal, String _Xil�fono, String _Campana, String _Cr�talos,
			String _Celesta, String _Caj�n, String _Tri�ngulo, String _Taiko, String _Casta�uelas, String _Maracas,
			String _Bombo, String _Bater�a, String _Gong, String _Pandereta) {
		super();
		this._Tambor = _Tambor;
		this._Timbal = _Timbal;
		this._Xil�fono = _Xil�fono;
		this._Campana = _Campana;
		this._Cr�talos = _Cr�talos;
		this._Celesta = _Celesta;
		this._Caj�n = _Caj�n;
		this._Tri�ngulo = _Tri�ngulo;
		this._Taiko = _Taiko;
		this._Casta�uelas = _Casta�uelas;
		this._Maracas = _Maracas;
		this._Bombo = _Bombo;
		this._Bater�a = _Bater�a;
		this._Gong = _Gong;
		this._Pandereta = _Pandereta;
	}
	
	//Get and Set
	public String get_Tambor() {
		return _Tambor;
	}
	public void set_Tambor(String _Tambor) {
		this._Tambor = _Tambor;
	}
	public String get_Timbal() {
		return _Timbal;
	}
	public void set_Timbal(String _Timbal) {
		this._Timbal = _Timbal;
	}
	public String get_Xil�fono() {
		return _Xil�fono;
	}
	public void set_Xil�fono(String _Xil�fono) {
		this._Xil�fono = _Xil�fono;
	}
	public String get_Campana() {
		return _Campana;
	}
	public void set_Campana(String _Campana) {
		this._Campana = _Campana;
	}
	public String get_Cr�talos() {
		return _Cr�talos;
	}
	public void set_Cr�talos(String _Cr�talos) {
		this._Cr�talos = _Cr�talos;
	}
	public String get_Celesta() {
		return _Celesta;
	}
	public void set_Celesta(String _Celesta) {
		this._Celesta = _Celesta;
	}
	public String get_Caj�n() {
		return _Caj�n;
	}
	public void set_Caj�n(String _Caj�n) {
		this._Caj�n = _Caj�n;
	}
	public String get_Tri�ngulo() {
		return _Tri�ngulo;
	}
	public void set_Tri�ngulo(String _Tri�ngulo) {
		this._Tri�ngulo = _Tri�ngulo;
	}
	public String get_Taiko() {
		return _Taiko;
	}
	public void set_Taiko(String _Taiko) {
		this._Taiko = _Taiko;
	}
	public String get_Casta�uelas() {
		return _Casta�uelas;
	}
	public void set_Casta�uelas(String _Casta�uelas) {
		this._Casta�uelas = _Casta�uelas;
	}
	public String get_Maracas() {
		return _Maracas;
	}
	public void set_Maracas(String _Maracas) {
		this._Maracas = _Maracas;
	}
	public String get_Bombo() {
		return _Bombo;
	}
	public void set_Bombo(String _Bombo) {
		this._Bombo = _Bombo;
	}
	public String get_Bater�a() {
		return _Bater�a;
	}
	public void set_Bater�a(String _Bater�a) {
		this._Bater�a = _Bater�a;
	}
	public String get_Gong() {
		return _Gong;
	}
	public void set_Gong(String _Gong) {
		this._Gong = _Gong;
	}
	public String get_Pandereta() {
		return _Pandereta;
	}
	public void set_Pandereta(String _Pandereta) {
		this._Pandereta = _Pandereta;
	}
	
}
