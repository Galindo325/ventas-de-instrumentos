package Clases;

public class Factura {
	
	//Atributos
	private Cliente _cliente;
	private Articulo _articulo;
	private String _NombreArticulo;
	private String _Codigo;
	private double _Precio;
	private String _Color;
	
	//Constructor
	public Factura(Cliente _cliente, Articulo _articulo, String _NombreArticulo, String _Codigo, double _Precio,
			String _Color) {
		super();
		this._cliente = _cliente;
		this._articulo = _articulo;
		this._NombreArticulo = _NombreArticulo;
		this._Codigo = _Codigo;
		this._Precio = _Precio;
		this._Color = _Color;
	}

	//Get and Set
	public Cliente get_cliente() {
		return _cliente;
	}

	

	public void set_cliente(Cliente _cliente) {
		this._cliente = _cliente;
	}

	public Articulo get_articulo() {
		return _articulo;
	}

	public void set_articulo(Articulo _articulo) {
		this._articulo = _articulo;
	}

	public String get_NombreArticulo() {
		return _NombreArticulo;
	}

	public void set_NombreArticulo(String _NombreArticulo) {
		this._NombreArticulo = _NombreArticulo;
	}

	public String get_Codigo() {
		return _Codigo;
	}

	public void set_Codigo(String _Codigo) {
		this._Codigo = _Codigo;
	}

	public double get_Precio() {
		return _Precio;
	}

	public void set_Precio(double _Precio) {
		this._Precio = _Precio;
	}

	public String get_Color() {
		return _Color;
	}

	public void set_Color(String _Color) {
		this._Color = _Color;
	}
		
}
