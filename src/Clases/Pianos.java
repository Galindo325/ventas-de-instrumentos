package Clases;

public class Pianos {
	
	//Atributos
	private String _DeCola;
	private String _DePared;
	private String _Electrónico;
	
	//Constructor
	public Pianos(String _DeCola, String _DePared, String _Electrónico) {
		super();
		this._DeCola = _DeCola;
		this._DePared = _DePared;
		this._Electrónico = _Electrónico;
	}
	
	//Set and Get
	public String get_DeCola() {
		return _DeCola;
	}
	public void set_DeCola(String _DeCola) {
		this._DeCola = _DeCola;
	}
	public String get_DePared() {
		return _DePared;
	}
	public void set_DePared(String _DePared) {
		this._DePared = _DePared;
	}
	public String get_Electrónico() {
		return _Electrónico;
	}
	public void set_Electrónico(String _Electrónico) {
		this._Electrónico = _Electrónico;
	}
	
}
