package Clases;

public class Instrumentos {
	
	//Atributos
	private Cuerda _Cuerda;
	private Viento _Viento;
	private Percusion _Percusion;
	private CuerdaFrotada _CuerdaFrotada;
	private Pianos _Pianos;
	
	//Constructor
	public Instrumentos(Cuerda _Cuerda, Viento _Viento, Percusion _Percusion, CuerdaFrotada _CuerdaFrotada,
			Pianos _Pianos) {
		super();
		this._Cuerda = _Cuerda;
		this._Viento = _Viento;
		this._Percusion = _Percusion;
		this._CuerdaFrotada = _CuerdaFrotada;
		this._Pianos = _Pianos;
	}

	
	//Get and Set
	public Cuerda get_Cuerda() {
		return _Cuerda;
	}

	public void set_Cuerda(Cuerda _Cuerda) {
		this._Cuerda = _Cuerda;
	}

	public Viento get_Viento() {
		return _Viento;
	}

	public void set_Viento(Viento _Viento) {
		this._Viento = _Viento;
	}

	public Percusion get_Percusion() {
		return _Percusion;
	}

	public void set_Percusion(Percusion _Percusion) {
		this._Percusion = _Percusion;
	}

	public CuerdaFrotada get_CuerdaFrotada() {
		return _CuerdaFrotada;
	}

	public void set_CuerdaFrotada(CuerdaFrotada _CuerdaFrotada) {
		this._CuerdaFrotada = _CuerdaFrotada;
	}

	public Pianos get_Pianos() {
		return _Pianos;
	}

	public void set_Pianos(Pianos _Pianos) {
		this._Pianos = _Pianos;
	}
	

	
	
	

}
