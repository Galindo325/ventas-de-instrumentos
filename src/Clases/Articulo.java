package Clases;

import Clases.*;
public class Articulo {
	
	//Atributos
	private Instrumentos _Instrumento;
	private Accesorios _Accesorio;
	
	//Constructor
	public Articulo(Instrumentos Instrumento, Accesorios Accesorio) {
		super();
		this._Instrumento = Instrumento;
		this._Accesorio = Accesorio;
	}
	
	//Get and Set
	public Instrumentos get_Instrumento() {
		return _Instrumento;
	}
	
	public void set_Instrumento(Instrumentos _Instrumento) {
		this._Instrumento = _Instrumento;
	}
	public Accesorios get_Accesorio() {
		return _Accesorio;
	}
	public void set_Accesorio(Accesorios _Accesorio) {
		this._Accesorio = _Accesorio;
	}
	
}
